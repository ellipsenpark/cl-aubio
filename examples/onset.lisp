;; simple example to extract onset times from an audio file.
;; modeled after the test-onset.c code found in the original
;; aubio library ... 

(ql:quickload :cl-aubio)
(in-package :cl-aubio)

(let* ((samplerate 0)
       (win_s 1024)
       (hop_size 256)
       (read_l 0)
       (read (cffi:foreign-alloc :int :initial-element read_l))
       (fname "/ABSOLUTE/PATH/TO/WAV/FILE") ;; <- insert your file here ... 
       (source (new_aubio_source fname samplerate hop_size))
       (onset))
  (cffi::with-foreign-objects ((in '(:struct fvec_t))
			       (out '(:struct fvec_t)))
    (setf in (new_fvec hop_size))
    (setf out (new_fvec 2))
    ;; not sure why this isn't done in the method above ...
    ;; but it's the same in the C example that served as
    ;; a template for this ... 
    (setf samplerate (aubio_source_get_samplerate source))
    (format t "samplerate: ~D~%" samplerate)
    ;; samplerate detection seems to be not so relieable, so set
    ;; the onset object can only be created now ... 
    (setf onset (new_aubio_onset "default" win_s hop_size samplerate))
    (loop named detect
       do (progn 
	    (aubio_source_do source in read)
	    (aubio_onset_do onset in out)	    
	    (when (not (eql
			(mem-aref (cffi::foreign-slot-value
				   out '(:struct fvec_t) 'data) ':float 0)
			0.0))
	      (format t "onset at: ~Dms, ~Ds, ~D frames ~%"
		      (aubio_onset_get_last_ms onset)
		      (aubio_onset_get_last_s onset)
		      (aubio_onset_get_last onset)))
	    (when (not (eql (mem-aref read ':int) 256))
	      (return-from detect))))
    ;; cleanup 
    (del_aubio_source source)
    (del_aubio_onset onset)
    (del_fvec in)
    (del_fvec out)
    (aubio_cleanup)))
