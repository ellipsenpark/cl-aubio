# cl-aubio

**cl-aubio** is a simple, SWIG-generated wrapper for the aubio [aubio](https://aubio.org) audio feature extraction library.

It's really not much more than a cffi interface so far. 

It's also heavily modeled after Gary Hollis' [cl-libsndfile](https://github.com/ghollisjr/cl-libsndfile) bindings.

Just clone this repository to your `quicklisp/local-projects` folder and import using `(ql-quickload :cl-aubio)`.

## Examples

* *onset_example.lisp* detects onsets in an audio file. Modeled after the onset test in the original aubio library.

## License

**cl-aubio** is licensed under the GPL v3 or later, just as the original aubio library.
