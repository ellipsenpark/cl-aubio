;;;; cl-aubio is a Common Lisp wrapper for aubio.
;;;; Copyright 2018 Niklas Reppel
;;;;
;;;; cl-aubio licensed under GPL v3 or later
(asdf:defsystem #:cl-aubio
  :serial t
  :author "Niklas Reppel"
  :description "cl-aubio is a Common Lisp wrapper for aubio"
  :license "GPLv3"
  :depends-on (#:cffi)
  :components ((:file "package")
               (:file "aubio")))
